const cacheName = 'onsen-rando';
const precacheResources = [
  '/',
  '/index.html',
  'https://unpkg.com/onsenui/css/onsenui.min.css',
  'https://unpkg.com/onsenui/css/onsen-css-components.min.css',
  'https://unpkg.com/onsenui/js/onsenui.min.js',
  'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css',
  'https://unpkg.com/leaflet@1.7.1/dist/leaflet.js'
];

self.addEventListener('install', (evt) => {
  evt.waitUntil(
    caches.open(cacheName).then(cache => cache.addAll(precacheResources))
  );
});

self.addEventListener('fetch', StaleWhileRevalidate);

// Cache uniquement
function CacheOnly(evt) {
  evt.respondWith(caches.match(evt.request));
}

// Réseau uniquement
function NetworkOnly(evt) {
  evt.respondWith(fetch(evt.request));
}

// En cache puis repli sur le réseau
// Cela vous donne le comportement "Cache uniquement" pour les éléments du cache et le comportement "Réseau uniquement" pour tout ce qui n'est pas mis en cache.
// Cela inclut toutes les demandes non GET, car elles ne peuvent pas être mises en cache.
function CacheFallbackNetwork(evt) {
  evt.respondWith(caches.match(evt.request).then(response => response || fetch(evt.request)));
}

// Réseau puis repli sur le cache
// Nous envoyons d'abord la requête au réseau à l'aide de fetch(), et ce n'est qu'en cas d'échec que nous recherchons une réponse dans le cache.
function NetworkFallbackCache(evt) {
  evt.respondWith(fetch(evt.request).catch(() => caches.match(evt.request)));
}

function CacheOnNetwork(evt)
{
  evt.respondWith(async function()
  {
    const cache = await caches.open(cacheName);
    const cachedResponse = await cache.match(evt.request);
    if (cachedResponse) return cachedResponse;

    const networkResponse = await fetch(evt.request);
    evt.waitUntil(
      cache.put(evt.request, networkResponse.clone())
    );
    return networkResponse;
  }());
}

// Périmé puis validation
// S'il existe une version en cache disponible, utilisez-la, mais récupérez une mise à jour pour la prochaine fois.
function StaleWhileRevalidate(evt)
{
  evt.respondWith(async function()
  {
    const cache = await caches.open(cacheName);
    const cachedResponse = await cache.match(evt.request);
    const networkResponsePromise = fetch(evt.request);

    evt.waitUntil(async function() {
      const networkResponse = await networkResponsePromise;
      await cache.put(evt.request, networkResponse.clone());
    }());

    return cachedResponse || networkResponsePromise;

  }());
}
