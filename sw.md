/* install */
self.addEventListener('install', function(e) {
  console.log('install sw.js');
  
  // waitUntil tells the browser that the install event is not finished until we have
  // cached all of our files
  e.waitUntil(
    // Here we call our cache "myonsenuipwa", but you can name it anything unique
    caches.open('myonsenuipwa').then(cache => {
      // If the request for any of these resources fails, _none_ of the resources will be
      // added to the cache.
      return cache.addAll([
        '/',
        '/index.html',
        'https://unpkg.com/onsenui/css/onsenui.min.css',
        'https://unpkg.com/onsenui/css/onsen-css-components.min.css',
        'https://unpkg.com/onsenui/js/onsenui.min.js',
        'https://unpkg.com/leaflet@1.7.1/dist/leaflet.css'
      ]);
    })
  );
});
  
/* fetch */
self.addEventListener('fetch', function(e) {
  
  e.respondWith(
    caches.open('myonsenuipwa').then(function (cache) {
      return cache.match(e.request).then(function (response) {
        let fetchPromise = fetch(e.request).then(function (networkResponse) {
            cache.put(e.request, networkResponse.clone());
            return networkResponse;
          });
        
        return response || fetchPromise;
      });
    }),
  );
});
